from todo_app.models import TodoList, Person, SharedInfo
from datetime import date

print("Adding data to django-db")
# Need protections against adding the same person twice
if Person.objects.filter(first_name='John', last_name='Smith').count() == 0:
    john = Person(first_name='John', last_name='Smith', creation_date=date(2022, 3, 1))
    john.save()
if Person.objects.filter(first_name='Jane', last_name='Smith').count() == 0:
    jane = Person(first_name='Jane', last_name='Smith', creation_date=date(2022, 3, 15))
    jane.save()
if Person.objects.filter(first_name='Bob', last_name='Jones').count() == 0:
    bob = Person(first_name='Bob', last_name='Jones', creation_date=date(2022, 4, 1))
    bob.save()

if TodoList.objects.filter(title='BOur vacation').count() == 0:
    our_vac = TodoList(title='Our vacation')
    our_vac.save()
    share = SharedInfo(person=jane, todo_list=our_vac, owner=john)
    