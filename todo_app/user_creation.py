from django.contrib.auth.models import User, Group, Permission
try:
    user = User.objects.create_user('test', 'test@email.com', 'python420')
    user.first_name = 'Test'
    user.last_name = 'User'
    user.save() # Should add protections later
except:
    pass

new_group, created = Group.objects.get_or_create(name='Viewable')
todo_list_perm = Permission.objects.get(codename='view_todolist')
new_group.permissions.add(todo_list_perm)
new_group.save()