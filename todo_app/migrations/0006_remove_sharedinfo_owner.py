# Generated by Django 4.0.3 on 2022-04-04 13:22

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('todo_app', '0005_sharedinfo_todolist_person_sharedinfo_todo_list'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='sharedinfo',
            name='owner',
        ),
    ]
