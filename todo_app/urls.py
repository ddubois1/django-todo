#todo_app/urls.py
from django.urls import path
from . import views
from django.contrib.auth.views import LoginView, LogoutView
urlpatterns = [
    path('', views.index, name='index'),
    path('list/<int:list_id>/', views.todo_list, name='todo_list'),
    path('image/<int:image_id>/', views.display_image, name='display-image'),
    path('upload_image/', views.add_my_image, name='add-image'),
    path('items/<int:pk>/', views.TodoListItemDetailView.as_view(), name='todolistitemdetail'),
    path('list/<int:list_id>/item/<int:item_id>/', views.todo_list_item, name='todo_list_item'),
    path('list/<int:list_id>/add_todo_item', views.add_todo_item, name='add-todo-item'),
    path('about/contact', views.ContactView.as_view(), name='contact'),
    path('login/', LoginView.as_view(template_name='todo_app/login.html'), name='login'),
    path('logout/', LogoutView.as_view(), name='logout'),
]