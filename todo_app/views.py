from django.urls import reverse
from datetime import date
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.template import loader
from django.shortcuts import HttpResponse

from todo_app.forms import AddTodoListItem, MyImageForm

from .models import AzureImage, TodoList, TodoListItem, MyImage

from django.views.generic.base import TemplateView
from django.views.generic.detail import DetailView

from django.contrib.auth.mixins import LoginRequiredMixin
class ContactView(LoginRequiredMixin, TemplateView):
    template_name = "todo_app/contact.html"
    login_url = '/login'
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['contact_num'] = "514-555-1234"
        return context

# Create your views here.
def index(request):
    items = TodoList.objects.order_by('-title')[:5]
    template = loader.get_template('todo_app/home.html')
    context = {
        'todo_list': items,
    }
    return HttpResponse(template.render(context, request))

from django.contrib.auth.decorators import login_required
@login_required(login_url='/login')
def todo_list(request, list_id):
    todo_list = TodoList.objects.get(id=list_id)
    todo_items = TodoListItem.objects.filter(todo_list=todo_list)
    template = loader.get_template('todo_app/todo_list.html')
    context = {
        'todo_list': todo_list,
        'todo_items' : todo_items,
    }
    return HttpResponse(template.render(context, request))

def todo_list_item(request, list_id, item_id):
    todo_list = TodoList.objects.get(id=list_id)
    todo_item = TodoListItem.objects.get(todo_list=todo_list, id=item_id)
    template = loader.get_template('todo_app/todo_list_item.html')
    context = {
        'todo_list': todo_list,
        'todo_item' : todo_item,
    }
    return HttpResponse(template.render(context, request))


def display_image(request, image_id):   
    image = AzureImage.objects.get(id=image_id)
    template = loader.get_template('todo_app/view_image.html')
    context = {
        'image_url': image.image.url,
    }
    return HttpResponse(template.render(context, request))

def add_my_image(request):
    form = None
    if request.method == 'POST':
        form = MyImageForm(request.POST, request.FILES)
        if form.is_valid():
            # Add to the database
            image_file = form.cleaned_data['image']
            from todo_app.image_utils import image_to_binary
            byte_arr = image_to_binary(image_file)
            item = MyImage(image=byte_arr)
            item.save()
            return HttpResponseRedirect(redirect_to=reverse('display-image', kwargs={'image_id':item.id}))
    else:
        form = MyImageForm()
    template = loader.get_template('todo_app/image_upload.html')
    context = {
        'form' : form
    }
    return HttpResponse(template.render(context, request))

def add_todo_item(request, list_id):
    form = None
    if request.method == 'POST':
        form = AddTodoListItem(request.POST)
        if form.is_valid():
            # Add to the database
            todo_list = TodoList.objects.get(id=list_id)
            
            title = form.cleaned_data['title']
            description = form.cleaned_data['description']
            due_date = form.cleaned_data['due_date']
            creation_date = date.today()
            
            item = TodoListItem(title=title, 
            description=description,
            due_date=due_date, 
            creation_date=creation_date, todo_list=todo_list)
            item.save()
    else:
        form = AddTodoListItem()
    template = loader.get_template('todo_app/todo_add_item.html')
    context = {
        'form' : form
    }
    return HttpResponse(template.render(context, request))

class TodoListItemDetailView(DetailView):
    model= TodoListItem
    template_name = 'todo_app/listitemdetail.html'