from django.contrib import admin
from todo_app.models import AzureImage, SharedInfo, TodoList, TodoListItem, Person
# Register your models here.
admin.site.register(TodoList)
admin.site.register(TodoListItem)
admin.site.register(Person)
admin.site.register(SharedInfo)
admin.site.register(AzureImage)