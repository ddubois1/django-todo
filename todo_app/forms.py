from django import forms
from datetime import datetime, date

class AddTodoListItem(forms.Form):
    title = forms.CharField()
    description = forms.CharField()
    due_date = forms.DateField()

    def clean_due_date(self):
        data = self.cleaned_data['due_date']
        #Validation
        if data < date.today():
            raise forms.ValidationError("Invalid due date")
        return data

class MyImageForm(forms.Form):
    image = forms.ImageField()