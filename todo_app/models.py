from distutils.command.upload import upload
from time import timezone
from django.db import models

# Create your models here.
class Person(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    creation_date = models.DateField(auto_now_add=True)
    
    def __str__(self):
        return f'{self.first_name} {self.last_name} ({self.id})'

class TodoList(models.Model):
    title = models.CharField(max_length=100)
    person = models.ManyToManyField(Person, through='SharedInfo', through_fields=('todo_list', 'person'))
    def __str__(self):
        return f'{self.title} ({self.id})'

class SharedInfo(models.Model):
    person = models.ForeignKey(Person, on_delete=models.CASCADE)
    todo_list = models.ForeignKey(TodoList, on_delete=models.CASCADE)
    owner = models.ForeignKey(Person, on_delete=models.CASCADE, related_name="owner")
    shared_date = models.DateField()
    def __str__(self) -> str:
        return f'{self.person} : {self.todo_list} : {self.owner}'

def one_week_hence():
    from django.utils import timezone
    return timezone.now() + timezone.timedelta(days=7)

def due_date_validation(due_date):
    from django.core.exceptions import ValidationError
    from django.utils import timezone
    if due_date <= timezone.now().date():
        raise ValidationError("Due date can't be before now")

class TodoListItem(models.Model):
    title = models.CharField(max_length=100)
    description = models.TextField(blank=True, null=True)
    creation_date = models.DateField(auto_now_add=True)
    due_date = models.DateField(default=one_week_hence, 
    validators=[due_date_validation])
    todo_list = models.ForeignKey(TodoList, on_delete=models.CASCADE)

    def __str__(self):
        return self.title

class AzureImage(models.Model):
    image = models.ImageField()

class MyImage(models.Model):
    image = models.BinaryField(editable=True)


    
